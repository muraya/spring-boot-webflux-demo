package com.techpoa.webflux;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoFluxTest {

    @Test
    public void testMono()
    {
      Mono<?> monoString =   Mono.just("sample muraya")
              .then(Mono.error(new RuntimeException("we have a problem")))
              .log();
      // error subscribe with error handling in place
      monoString.subscribe(System.out::println, (e) -> System.out.println(e.getMessage()));
    }

    @Test
    public void testFlux()
    {
       Flux<?> flusSample =  Flux.just("we" , "are" , "headed" , "far")
               .concatWithValues("AWS")
               .log();
       flusSample.subscribe(System.out::println);
    }

}
